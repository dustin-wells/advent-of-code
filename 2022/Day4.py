with open("sample/4.txt") as f:
    data = f.read().split("\n")[:-1]

total = 0
partial = 0
for line in data:
    pair = {}
    pair["elf1"], pair["elf2"] = line.split(",")
    for elf in pair:
        start, end = map(int, pair[elf].split("-"))
        pair[elf] = set(range(start,end+1))
    if pair["elf1"].difference(pair["elf2"]) == set() or pair["elf2"].difference(pair["elf1"]) == set():
        total += 1
        partial += 1
    elif pair["elf1"].intersection(pair["elf2"]) != set():
        partial +=1

if __name__ == "__main__":
    print("Answer 1:", total, sep="\t")
    print("Answer 2:", partial, sep="\t")