with open("sample/3.txt") as f:
    data = f.read().split("\n")[:-1]

priority="*abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

indiv_total = group_total = 0

for sack in data:
    items = len(sack)
    comp1, comp2 = sack[:items//2], sack[items//2:]
    dupe = list(set(comp1).intersection(set(comp2)))[0]
    prior_val = priority.index(dupe)
    indiv_total += prior_val

for i in range(0,len(data),3):
    badge = list(set(data[i]).intersection(set(data[i+1])).intersection(set(data[i+2])))[0]
    prior_val = priority.index(badge)
    group_total += prior_val

if __name__ == "__main__":
    print("Answer 1:", indiv_total, sep="\t")
    print("Answer 2:", group_total, sep="\t")