with open("sample/6.txt") as f:
    data = f.read().split("\n")[0]

def find_marker(num_chars):
    for i in range(num_chars,len(data)):
        marker = data[i-num_chars:i]
        if len(set(marker)) == num_chars:
            return i
    return -1

if __name__ == "__main__":
    print("Answer 1:", find_marker(4), sep="\t")
    print("Answer 2:", find_marker(14), sep="\t")