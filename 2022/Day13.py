import json

def read_input(file):
    with open(file) as f:
        data = f.read()[:-1]
    return data

def comp_item(item1, item2):
    if type(item1) == int and type(item2) == list:
        item1 = [item1]
        return comp_item(item1, item2)
    elif type(item1) == list and type(item2) == int:
        item2 = [item2]
        return comp_item(item1, item2)
    elif type(item1) == list and type(item2) == list:
        for i in range(len(item1)):
            try:
                comparison = comp_item(item1[i], item2[i])
            except IndexError:
                return 1
            if comparison == -1:
                return -1
            elif comparison == 1:
                return 1
        if len(item2) > len(item1):
            return -1
        else:
            return 0
    else:
        if item1 < item2:
            return -1
        elif item1 == item2:
            return 0
        else:
            return 1

def comp(list1, list2):
    l1 = json.loads(list1)
    l2 = json.loads(list2)
    for i in range(len(l1)):
        try:
            comparison = comp_item(l1[i], l2[i])
        except IndexError:
            return False
        if comparison == 1:
            return False
        elif comparison == -1:
            return True
    if len(l2) > len(l1):
        return True
    else:
        raise ValueError("No determination made for:\t"+list1+"\t"+list2)


def main(file="..\\aoc-inputs\\2022\\13.txt"):      # ..\\aoc-inputs\\2022\\13.txt
    data = read_input(file)
    
    # Part 1
    data1 = data.split("\n\n")
    total = []
    for i in range(len(data1)):
        p1, p2 = data1[i].split("\n")
        if comp(p1, p2) == True:
            total.append(i+1)

    # Part 2
    data2 = data.replace("\n\n", "\n").split("\n")
    div1 = 1
    div2 = 2
    for i in data2:
        if comp(i, '[[2]]') == True:
            div1 += 1
        if comp(i, '[[6]]') == True:
            div2 += 1
    print(div1, div2)
    return (sum(total), div1 * div2)

if __name__ == "__main__":
    part1, part2 = main()
    print("Answer 1:", part1)
    print("Answer 2:", part2)

    # 4370 < part1 < 11182