with open("sample/2.txt") as f:
    data = f.read().split("\n")[:-1]

def part1():
    score=0
    for i in data:
        match i[2]:
            case "X":   # Rock
                score += 1
                if i[0] == "A":     # Rock (Draw)
                    score += 3
                elif i[0] == "B":   # Paper (Lose)
                    score += 0
                else:               # Scissors (Win) 
                    score += 6
            case "Y":   # Paper
                score += 2
                if i[0] == "A":     # Rock (Win)
                    score += 6
                elif i[0] == "B":   # Paper (Draw)
                    score += 3
                else:               # Scissors (Lose)
                    score += 0
            case "Z":   # Scissors
                score += 3
                if i[0] == "A":     # Rock (Lose)
                    score += 0
                elif i[0] == "B":   # Paper (Win)
                    score += 6
                else:               # Scissors (Draw)
                    score += 3
    return score

def part2():
    score=0
    for i in data:
        match i[2]:
            case "X":   # Lose
                score += 0
                if i[0] == "A":     # Rock (So I have Scissors)
                    score += 3
                elif i[0] == "B":   # Paper (So I have Rock)
                    score += 1
                else:               # Scissors (So I have Paper)
                    score += 2
            case "Y":   # Draw
                score += 3
                if i[0] == "A":     # Rock (So I have Rock)
                    score += 1
                elif i[0] == "B":   # Paper (So I have Paper)
                    score += 2
                else:               # Scissors (So I have Scissors)
                    score += 3
            case "Z":   # Win
                score += 6
                if i[0] == "A":     # Rock (So I have Paper)
                    score += 2
                elif i[0] == "B":   # Paper (So I have Scissors)
                    score += 3
                else:               # Scissors (So I have Rock)
                    score += 1
    return score

if __name__ == "__main__":
    print("Answer 1:", part1(), sep="\t")
    print("Answer 2:", part2(), sep="\t")