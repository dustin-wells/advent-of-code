# from ttp import ttp

monkey_template = """
<group name='{{ num }}'>
Monkey {{ num | to_int }}:
  Starting items: {{ items }}
  Operation: new = old {{ op | sformat('(i {}) // 3') }}
  Test: divisible by {{ test | sformat('i mod {} == 0') }}
    If true: throw to monkey {{ true_monkey }}
    If false: throw to monkey {{ false_monkey }}
</group>
"""

def read_input(file):
    with open(file) as f:
        data = f.read().split("\n")
    # Decided not to use ttp as it wasn't saving the starting items
    # parser = ttp(data, monkey_template)
    # parser.parse()
    # ret = parser.result()[0][0] 
    return data

class monkey:
    def __init__(self, items, op, test, next_true, next_false, worry, lcm):
        self.items = items
        self.op = op
        self.test = test
        self.true = next_true
        self.false = next_false
        self.inspect_count = 0
        self.worry = worry
        self.lcm = lcm
    
    def inspect_items(self, monkeys):
        while len(self.items) > 0:
            i = self.items.pop(0)   # Get the first item
            if not self.worry:
                i = eval(self.op) // 3  # Update worry level
            else:
                i = eval(self.op) % self.lcm # Make my PC cry less
            if eval(self.test):
                monkeys[self.true].items.append(i)
            else:
                monkeys[self.false].items.append(i)
            self.inspect_count += 1


def build_monkeys(data, worry):
    monkeys = []
    lcm = 1
    for i in range(0, len(data), 7): # step 7 lines per monkey with the spacer
        # monkey_num = data[i][7:-1]
        items = list(map(int, data[i+1][18:].split(", ")))
        op = data[i+2][18:].replace("old", "i")
        if data[i+3][8:20] == "divisible by":
            test_num = data[i+3][21:]
            test = "i % " + test_num + " == 0"
            lcm *= int(test_num)
        else:
            raise TypeError("Unknown test type!")
        next_true = int(data[i+4][29:])
        next_false = int(data[i+5][30:])
        monkeys.append(monkey(items, op, test, next_true, next_false, worry, lcm))
    for m in monkeys:
        m.lcm = lcm
    return monkeys

def part1():
    return main(file="sample/11.txt", rounds=20, worry=False)

def part2():
    return main(file="sample/11.txt", rounds=10000, worry=True)

def main(file, rounds, worry):
    data = read_input(file)

    # Initialize monkeys array
    monkeys = build_monkeys(data, worry)

    # Loop of X rounds of shenanigans
    for r in range(rounds):
        for monkey in monkeys:
            monkey.inspect_items(monkeys)
    # Indent to print after each round instead
    # print("End of round ", r, ":", sep="")
    # for m in range(len(monkeys)):
    #     print("Monkey ", m, ": ", monkeys[m].items, sep="")
    
    inspected = []
    for m, monkey in enumerate(monkeys):
        #print("Monkey ", m, ": ", monkey.inspect_count, sep="")
        inspected.append(monkey.inspect_count)
    
    monkey_business = sorted(inspected)[-1] * sorted(inspected)[-2]

    return monkey_business

if __name__ == "__main__":
    print("Answer 1:", part1())
    print("Answer 2:", part2())