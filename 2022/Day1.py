with open("sample/1.txt") as f:
    data = f.read()

elves = data.split("\n\n")
for i, food in enumerate(elves):
    elves[i] = sum(food.split("\n"))

elves.sort()
most = elves[-1]
top3 = sum(elves[-3:])

if __name__ == "__main__":
    print("Answer 1:", most, sep="\t")
    print("Answer 2:", top3, sep="\t")