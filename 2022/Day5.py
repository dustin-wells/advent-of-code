import re

with open("sample/5.txt") as f:
    data = f.read().split("\n")[:-1]

stack_num = (len(data[0]) + 1) // 4
for i in range(len(data)):
    if len(data[i]) != len(data[0]):
        stack_height = i - 1
        break

positions = ['-', 1]
stacks = {}
for i in range(1, stack_num + 1):       # Build the initial stack arrays
    if i != 1:
        positions.append(positions[-1] + 4)
    stacks.update({i:[]})
    for line in data[:stack_height]:
        char = line[positions[i]]
        if char != " ":
            stacks[i].append(char)
    stacks[i].reverse()

def part1():
    stacks = stacks.copy()
    for i in range(stack_height+2,len(data)):
        num, frm, to = map(int, re.findall(r'\d+', data[i]))
        for i in range(num):
            m = stacks[frm].pop()
            stacks[to].append(m)
    answer = ''
    for i in range(1,stack_num + 1):
        answer += stacks[i][-1]
    return answer

def part2():
    stacks = stacks.copy()
    for i in range(stack_height+2,len(data)):
        num, frm, to = map(int, re.findall(r'\d+', data[i]))
        stacks[frm], move = stacks[frm][:-num], stacks[frm][-num:]
        for m in move:
            stacks[to].append(m)
    answer = ''
    for i in range(1,stack_num + 1):
        answer += stacks[i][-1]
    return answer

if __name__ == "__main__":
    print("Answer 1:", part1(), sep="\t")
    print("Answer 2:", part2(), sep="\t")