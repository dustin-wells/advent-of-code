with open("sample/8.txt") as f:
    data = f.read().split("\n")[:-1]

def colored(r, g, b, text):
    return "\033[38;2;{};{};{}m{}\033[38;2;255;255;255m".format(r, g, b, text)

grid = []
for r, line in enumerate(data):
    row = []
    for col, tree in enumerate(line):
        row.append({
            "row":r,
            "col":col,
            "height":int(tree),
            "visible": False
        })
    grid.append(row)

def check_left(tree):
    trees = 0
    for i in range(tree["col"] - 1, -1, -1):
        trees += 1
        if grid[tree["row"]][i]["height"] >= tree["height"]:
            break
    return trees

def check_right(tree):
    trees = 0
    for i in range(tree["col"]+1, len(grid[tree["row"]]), 1):
        trees += 1
        if grid[tree["row"]][i]["height"] >= tree["height"]:
            break
    return trees

def check_up(tree):
    trees = 0
    for i in range(tree["row"] - 1, -1, -1):
        trees += 1
        if grid[i][tree["col"]]["height"] >= tree["height"]:
            break
    return trees

def check_down(tree):
    trees = 0
    for i in range(tree["row"]+1, len(grid), 1):
        trees += 1
        if grid[i][tree["col"]]["height"] >= tree["height"]:
            break
    return trees

def check_visible(tree):
    if tree["row"] == 0 or tree["row"] == len(grid):
        tree["visible"] = True      # top or bottom border
    elif tree["col"] == 0 or tree["col"] == len(grid[tree["row"]]):
        tree["visible"] = True      # left or right border
    elif tree["row"] == check_up(tree):
        tree["visible"] = True     # visible from the top
    elif len(grid) == check_down(tree) + 1:
        tree["visible"] = True     # visible from the bottom
    elif tree["col"] == check_left(tree):
        tree["visible"] = True     # visible from the left
    elif len(grid[tree["row"]]) == check_right(tree) + 1:
        tree["visible"] = True     # visible from the right
    return tree["visible"]

def calculate_score(tree):
    down = check_down(tree)
    up = check_up(tree)
    left = check_left(tree)
    right = check_right(tree)
    score = down * up * left * right
    # print(tree["row"], tree["col"], up, right, down, left, score, sep="\t")
    return score

def part1():
    visible = 0
    for row in grid:
        for tree in row:
            if check_visible(tree):
                visible += 1
    return visible

def part2():
    high = 0
    # print("row", "col", "up", "right", "down", "left", "score", sep="\t")
    for row in grid:
        for tree in row:
            score = calculate_score(tree)
            if score > high:
                high = score
                high_tree = (tree["row"], tree["col"])
    # print(high, high_tree, sep="\t")
    return high

if __name__ == "__main__":
    print("Answer 1:", part1(), sep="\t")
    print("Answer 2:", part2(), sep="\t")