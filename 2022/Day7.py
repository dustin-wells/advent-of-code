with open("sample/7.txt") as f:
    data = f.readlines()

all_folders = {}

class folder:
    def __init__(self, name, parent=None):
        self.name = name
        self.size = 0
        self.children = {}
        self.parent = parent
        if parent == None:
            all_folders.update({name: self})
        else:
            all_folders.update({"/".join([parent.name,name]): self})
    
    def update_size(self,size):
        self.size += size
        if self.parent != None:
            self.parent.update_size(size)

    def add_file(self, name, size):
        self.children.update({name: int(size)})
        self.update_size(int(size))
    
    def add_folder(self, name):
        self.children.update({name: folder(name, self)})

class fs:
    root = folder("/")
    current_dir = root

    def cd(self, dir):
        if dir == "/":
            self.current_dir = self.root
        elif dir == "..":
            self.current_dir = self.current_dir.parent
        else:
            self.current_dir = self.current_dir.children[dir]

    def process(self, data):
        for line in data:
            if line[:4] == "$ cd":
                self.cd(line[5:-1])
            elif line[:4] == "dir ":
                self.current_dir.add_folder(line[4:-1])
            elif line == "$ ls\n":
                continue
            else:
                size, name = line.strip().split(" ")
                self.current_dir.add_file(name, size)

def main():
    nix = fs()
    nix.process(data)

    total_space = 70000000
    req_space = 30000000
    taken_space = all_folders["/"].size
    space_left = total_space - taken_space
    still_needed = req_space - space_left
    min_val = taken_space
    # print(taken_space, still_needed, sep="\t")

    # part 1
    micro_space = 0
    for i in all_folders.values():
        if i.size <= 100000:        # Part 1
            micro_space += i.size
        if i.size >= still_needed and i.size < min_val:     # Part 2
            min_val = i.size

    return micro_space, min_val

if __name__ == "__main__":
    space, size = main()
    print("Answer 1: ", space, sep="")
    print("Answer 2: ", size, sep="")