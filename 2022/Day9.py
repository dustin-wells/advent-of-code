def read_input(file):
    with open(file) as f:
        data = f.readlines()
    return data

class knot:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.visited = [(x, y)]
    
    @property
    def location(self):
        return (self.x, self.y)
    
    def move(self, dir):
        if dir == "R":
            self.x += 1
        elif dir == "L":
            self.x -= 1
        elif dir == "U":
            self.y += 1
        elif dir == "D":
            self.y -= 1
        else:
            raise ValueError("Invalid Direction")
        self.visited.append(self.location)
    
    def pull(self, other):
        if other.x == self.x + 2:
            self.x += 1
            if self.y < other.y:
                self.y += 1
            elif self.y > other.y:
                self.y -= 1
        elif other.x == self.x - 2:
            self.x -= 1
            if self.y < other.y:
                self.y += 1
            elif self.y > other.y:
                self.y -= 1
        elif other.y == self.y + 2:
            self.y += 1
            if self.x < other.x:
                self.x += 1
            elif self.x > other.x:
                self.x -= 1
        elif other.y == self.y - 2:
            self.y -= 1
            if self.x < other.x:
                self.x += 1
            elif self.x > other.x:
                self.x -= 1
        self.visited.append(self.location)

def draw(rope):
    ret = ""
    for y in range(-4, 1):
        row = ""
        for x in range(0, 6):
            for i in range(len(rope)):
                if x == rope[i].x and y == rope[i].y:
                    row += str(i)
                    continue
            if x == 0 and y == 0:
                row += "s"
            elif (x, y) in rope[-1].visited:
                row += "#"
            else:
                row += "."
        ret += row + "\n"
    return ret

def main(file="sample/9.txt", num=2):
    moves = read_input(file)

    rope = []
    for i in range(num):
        rope.append(knot(0,0))
    
    for m, move in enumerate(moves):
        # print("====Move ", m, "====   ", move, sep='')
        dir, times = move.split(" ")
        times = int(times)
        header = "knot #\t-Before-"
        positions = []
        for t in range(times):
            header += "\t-Loop "+str(t)+"-"
            for i, k in enumerate(rope):
                if t == 0:
                    positions.append([])
                positions[i].append(str(k.location))
            rope[0].move(dir)
            for n in range(1,len(rope)):
                rope[n].visited.append(rope[n].location)
                rope[n].pull(rope[n-1])
        for i, k in enumerate(rope):
            positions[i].append(str(k.location))

        # print(header)
        # for num, kn in enumerate(positions):
        #     print(num,"\t\t".join(kn),sep="\t")
        # print()
    
    #print(draw(rope))

    # for i, k in enumerate(rope):
    #     print(i, len(set(k.visited)), sep="\t")
    
    return len(set(rope[-1].visited))

if __name__ == "__main__":
    print("Answer 1:", main(), sep="\t")
    print("Answer 2:", main(num=10), sep="\t")
    # Use the larger sample data for step 2
    print("Answer 2b:", main(file="sample/9-2.txt", num=10), sep="\t")