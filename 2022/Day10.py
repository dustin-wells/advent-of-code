def read_input(file):
    with open(file) as f:
        data = f.readlines()
    return data

class circuit:
    def __init__(self):
        self.X = 1
        self.cycle = 1
        self.draw_position = 0
        self.nums = [20, 60, 100, 140, 180, 220]
        self.vals = []
        self.CRT = ""

    def do_cycle(self, num):
        for i in range(num):
            if self.draw_position > self.X-2 and self.draw_position < self.X+2:
                self.CRT += "#"
            else:
                self.CRT += '.'
            if self.cycle in self.nums:
                self.vals.append(self.X * self.cycle)
            self.cycle += 1
            self.draw_position += 1
            if self.cycle % 40 == 1:
                self.CRT += "\n"
                self.draw_position = 0
        return

    def do(self, cmd):
        if cmd.lower()[:4] == "noop":
            self.do_cycle(1)
            return
        if cmd.lower()[:4] != "addx":
            raise ValueError("Invalid Instruction!")
        self.do_cycle(2)
        self.X += int(cmd[5:])

def main(file="sample/10.txt", func=sum):
    cmds = read_input(file)
    crt = circuit()

    for cmd in cmds:
        crt.do(cmd)
    
    print()
    # print(crt.vals)
    print("Answer 1:",func(crt.vals))
    print("Answer 2:")
    print(crt.CRT)
    print()
    

if __name__ == "__main__":
    main()